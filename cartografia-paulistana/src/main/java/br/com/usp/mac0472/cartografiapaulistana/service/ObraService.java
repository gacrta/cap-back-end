package br.com.usp.mac0472.cartografiapaulistana.service;

import java.util.*;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import br.com.usp.mac0472.cartografiapaulistana.model.Construtora;
import br.com.usp.mac0472.cartografiapaulistana.model.Endereco;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.model.Referencia;
import br.com.usp.mac0472.cartografiapaulistana.repository.ObraRepository;
import br.com.usp.mac0472.cartografiapaulistana.repository.ReferenciaRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;

@Service
public class ObraService {

	@Autowired
	private ObraRepository repository;
	
	@Autowired
	private ConstrutoraService construtoraService;
	
	@Autowired
	private ArquitetoService arquitetoService;
	
	@Autowired
	private ReferenciaRepository referenciaRepository;

	public Page<Obra> readObras(Pageable pageable) {
		return repository.findObras(pageable);
	}

	public Page<Obra> readObrasWithValidation(Pageable pageable, Boolean validadasProfessora, Boolean validadasDph) {
		return repository.findObrasWithValidation(pageable, validadasProfessora, validadasDph);
	}

	public Optional<Obra> readObra(Integer id) {
		return repository.findById(id);
	}

	@Transactional
	public Obra createObra(Obra obra, List<String> arquitetosId, Integer construtoraId, Integer arquitetoReformaId, Endereco endereco, List<String> referenciasUrls) {
		if(construtoraId != null)
		{
			Construtora construtora = construtoraService.readConstrutora(construtoraId)
					.orElseThrow(() -> new EntityNotFoundException("Construtora não encontrada."));
			obra.setConstrutora(construtora);
		}
		else {
			obra.setConstrutora(null);
		}
		Set<Arquiteto> arquitetos = Set.copyOf(arquitetosId.stream().map(autoriaId -> {
			return arquitetoService.readArquiteto(Integer.parseInt(autoriaId)).orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
		}).toList());
		if(arquitetoReformaId != null)
		{
			Arquiteto arquitetoReforma = arquitetoService.readArquiteto(arquitetoReformaId)
					.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
			obra.setArquitetoReforma(arquitetoReforma);
		}
		List<Referencia> referencias = referenciasUrls.stream().map(referenciaUrl -> new Referencia(referenciaUrl)).toList();
		obra.setArquitetos(arquitetos);
		obra.setValidadoDPH(false);
		obra.setValidadoProfessora(false);
		obra.setEndereco(endereco);
		Obra obraSalva = repository.save(obra);
		referencias.forEach(referencia -> referencia.setObra(obraSalva));
		referenciaRepository.saveAll(referencias);
		return obraSalva;
		
	}

	@Transactional
	public Optional<Obra> updateObra(Integer id, ObraUpdateDto updatedObra, Endereco endereco) {
		Obra existingObra = repository.getReferenceById(id);
		List<Referencia> existingReferencias = existingObra.getReferencias().stream().toList();
		if(updatedObra.construtora() != null)
		{
			Construtora construtora = construtoraService.readConstrutora(updatedObra.construtora())
					.orElseThrow(() -> new EntityNotFoundException("Construtora não encontrada."));
			existingObra.setConstrutora(construtora);
		}
		else {
			existingObra.setConstrutora(null);
		}
		if(updatedObra.arquitetoReforma() != null)
		{
			Arquiteto arquitetoReforma = arquitetoService.readArquiteto(updatedObra.arquitetoReforma())
					.orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
			existingObra.setArquitetoReforma(arquitetoReforma);
		}
		List<Referencia> referencias = updatedObra.referencias().stream().map(referenciaUrl -> new Referencia(referenciaUrl)).toList();
		Set<Arquiteto> arquitetos = Set.copyOf(updatedObra.autoria().stream().map(autoriaId -> {
			return arquitetoService.readArquiteto(Integer.parseInt(autoriaId)).orElseThrow(() -> new EntityNotFoundException("Arquiteto não encontrado."));
		}).toList());
		existingObra.setEndereco(endereco);
		existingObra.update(updatedObra);
		existingObra.getArquitetos().clear();
		existingObra.getArquitetos().addAll(arquitetos);
		repository.save(existingObra);
		existingReferencias.forEach(referencia -> referenciaRepository.delete(referencia));
		referencias.forEach(referencia -> referencia.setObra(existingObra));
		referenciaRepository.saveAll(referencias);
		return Optional.ofNullable(existingObra);
	}

	@Transactional
	public void deleteObra(Integer id) {
		repository.deleteById(id);
	}

	@Transactional
	public Obra validacaoProfessora(Integer id) {
		Obra existingObra = repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		existingObra.setValidadoProfessora(!existingObra.getValidadoProfessora());
		repository.save(existingObra);
		return existingObra;
	}

	@Transactional
	public Obra validacaoDPH(Integer id) {
		Obra existingObra = repository.findById(id).orElseThrow(() -> new EntityNotFoundException());
		existingObra.setValidadoDPH(!existingObra.getValidadoDPH());
		repository.save(existingObra);
		return existingObra;
	}
}
