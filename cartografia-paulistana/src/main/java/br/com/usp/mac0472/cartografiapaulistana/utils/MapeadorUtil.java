package br.com.usp.mac0472.cartografiapaulistana.utils;

import java.util.List;

import br.com.usp.mac0472.cartografiapaulistana.model.Arquiteto;
import org.modelmapper.ModelMapper;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraPageResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Endereco;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import io.micrometer.common.util.StringUtils;

public abstract class MapeadorUtil {

	public static ObraPageResponseDto mapObraToPageResponse(Obra obra, ModelMapper mapper) {
		ObraPageResponseDto obraResponse = mapper.map(obra, ObraPageResponseDto.class);
		List<Integer> idsArquitetos = getIdsArquitetos(obra);
		obraResponse.setAnoProjeto(obra.getAnoProjeto());
		obraResponse.setEndereco(getEnderecoResponse(obra));
		obraResponse.setAutoria(idsArquitetos);
		return obraResponse;
	}
	
	public static ObraResponseDto mapObraToObraResponse(Obra obra, ModelMapper mapper) {
		ObraResponseDto obraResponse = mapper.map(obra, ObraResponseDto.class);
		List<Integer> idsArquitetos = getIdsArquitetos(obra);
		if(obra.getConstrutora() != null){
			String construtoraId = obra.getConstrutora().getId().toString();
			obraResponse.setConstrutora(construtoraId);
		}
		List<String> referencias = obra.getReferencias().stream().map(referencia -> referencia.getUrl()).toList();
		EnderecoResponseDto enderecoResponse = getEnderecoResponse(obra);
		obraResponse.setAutoria(idsArquitetos);
		obraResponse.setReferencias(referencias);
		obraResponse.setEndereco(enderecoResponse);
		if(obra.getArquitetoReforma() != null)
			obraResponse.setArquitetoReforma(obra.getArquitetoReforma().getId().toString());
		return obraResponse;
	}

	private static List<Integer> getIdsArquitetos(Obra obra)
	{
		List<Integer> idsArquitetos = obra.getArquitetos().stream()
				.map(Arquiteto::getId)
				.toList();
		return idsArquitetos;
	}

	private static List<String> getNomesArquitetos(Obra obra){
		List<String> nomesArquitetos = obra.getArquitetos().stream()
				.map(arquiteto -> 
				new StringBuilder()
				.append(arquiteto.getNome())
				.append(StringUtils.isNotBlank(arquiteto.getNomeMeio()) ? " " + arquiteto.getNomeMeio() + " " : " ")
				.append(arquiteto.getSobrenome())
				.toString())
				.toList();
		return nomesArquitetos;
	}
	
	private static EnderecoResponseDto getEnderecoResponse(Obra obra){
		Endereco endereco = obra.getEndereco();
		var enderecoResponse = new EnderecoResponseDto();
		enderecoResponse.setTipoEndereco(endereco.getTipoEndereco());
		enderecoResponse.setEnderecoTitulo(endereco.getEnderecoTitulo());
		enderecoResponse.setLogradouro(endereco.getLogradouro());
		enderecoResponse.setNumero(endereco.getNumero());
		enderecoResponse.setComplemento(endereco.getComplemento());
		enderecoResponse.setCep(endereco.getCep());
		enderecoResponse.setMunicipio(endereco.getMunicipio());
		return enderecoResponse;
	}
	
}
