package br.com.usp.mac0472.cartografiapaulistana.dto.obra;

import java.util.List;

public record ObraUpdateDto(
		String latitude, 
		String longitude, 
		String nomeOficial,
		List<String> autoria,
		Integer anoProjeto,
		Integer anoConstrucao, 
		Integer condephaat, 
		Integer conpresp,
		Integer iphan,
		String usoOriginal,
		String codigoOriginal,
		String usoAtual,
		String codigoAtual,
		String status,
		String escritorio,
		String nomeAlternativo,
		Integer construtora,
		Integer dataUsoAtual,
		Integer anoDemolicao,
		Integer anoRestauro,
		Integer anoReforma,
		Integer arquitetoReforma,
		List<String> referencias,
		EnderecoCreateDto endereco
		) {

}
