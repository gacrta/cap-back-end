package br.com.usp.mac0472.cartografiapaulistana.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.usp.mac0472.cartografiapaulistana.utils.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.usp.mac0472.cartografiapaulistana.dto.auth.LoginResponseDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.usuario.UserCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.usuario.UserLoginDto;
import br.com.usp.mac0472.cartografiapaulistana.model.Usuario;
import br.com.usp.mac0472.cartografiapaulistana.repository.UserRepository;
import br.com.usp.mac0472.cartografiapaulistana.service.TokenService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*")
public class AutenticacaoController {

	@Autowired
	private UserRepository repository;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping("/login")
	public ResponseEntity<LoginResponseDto> login(@RequestBody UserLoginDto data) {
		var usernamePassword = new UsernamePasswordAuthenticationToken(data.login(), data.senha());
		var auth = this.authenticationManager.authenticate(usernamePassword);
		var token = tokenService.generateToken((Usuario) auth.getPrincipal());
		
		Usuario usuario = (Usuario) auth.getPrincipal();
		
		var response = new LoginResponseDto(usuario.getId(), usuario.getLogin(), token, usuario.getAuthorities().toString());

		return ResponseEntity.ok(response);
	}

	@PostMapping("/cadastro")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid UserCreateDto data) throws URISyntaxException {
		List<String> errorMessages = new ArrayList<String>();
		if (Objects.nonNull(this.repository.findByLogin(data.login()))) {
			String error = "Já existe um usuário cadastrado com esse login";
			errorMessages.add(error);
		}
		if (Objects.nonNull(this.repository.findByEmail(data.email()))) {
			String error = "Já existe um usuário cadastrado com esse e-mail";
			errorMessages.add(error);
		}
		if (Objects.nonNull(this.repository.findByNumeroUsp(data.numeroUsp()))) {
			String error = "Já existe um usuário cadastrado com esse número USP";
			errorMessages.add(error);
		}

		if (!errorMessages.isEmpty()) {
			ApiError apiError =
					new ApiError(HttpStatus.BAD_REQUEST, "Erro ao criar usuário." , errorMessages);


			return new ResponseEntity<Object>(
					apiError, new HttpHeaders(), apiError.getStatus());
		}

		String senhaEncriptada = new BCryptPasswordEncoder().encode(data.senha());
		Usuario usuarioSalvo = this.repository.save(new Usuario(data, senhaEncriptada));
		return ResponseEntity.created(new URI("/api/usuarios/" + usuarioSalvo.getId())).build();
	}
}
