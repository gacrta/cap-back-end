ALTER TABLE obra
    ADD COLUMN arquiteto_reforma_id INT,
ADD CONSTRAINT fk_obra_arquiteto
    FOREIGN KEY (arquiteto_reforma_id)
    REFERENCES arquiteto(id);

ALTER TABLE obra ADD COLUMN ano_reforma INT;