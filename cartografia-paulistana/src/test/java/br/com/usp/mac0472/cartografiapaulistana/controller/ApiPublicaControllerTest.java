package br.com.usp.mac0472.cartografiapaulistana.controller;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;

import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ObraBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.service.ObraService;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ApiPublicaControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ObraService service;

    @Test
    public void givenObraCadastrada_whenGetObras_thenReturnValidatedObras() throws Exception {

        // Given
        PageRequest paginacao = PageRequest.of(0, 1);
        Obra obra = new ObraBuilder().build();
        List<Obra> lista_obras = new ArrayList<Obra>(Arrays.asList(obra));
        Page<Obra> result = new PageImpl<Obra>(lista_obras, paginacao, lista_obras.size());

        when(service.readObrasWithValidation(any(), any(), any()))
                .thenReturn(result);

        // Then
        mvc.perform(get("/api/publica/obras"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(obra.getId())));
    }
}
