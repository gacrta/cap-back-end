package br.com.usp.mac0472.cartografiapaulistana.controller;

import br.com.usp.mac0472.cartografiapaulistana.dto.obra.EnderecoCreateDto;
import br.com.usp.mac0472.cartografiapaulistana.dto.obra.ObraUpdateDto;
import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ObraUpdateDtoBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import br.com.usp.mac0472.cartografiapaulistana.helper.builder.ObraBuilder;
import br.com.usp.mac0472.cartografiapaulistana.model.Obra;
import br.com.usp.mac0472.cartografiapaulistana.model.Endereco;
import br.com.usp.mac0472.cartografiapaulistana.service.ObraService;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ObraControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ObraService service;

    /* TESTES POST /api/obras */


    @Test
    public void givenObra_whenCreateObra_thenReturnObra() throws Exception {

        // Given
        ObraBuilder builder = new ObraBuilder();
        String json = builder.jsonBuild();
        Obra obra = builder.build();
        assertNotNull(json, "Json vazio!");
        assertNotNull(obra, "Obra vazia!");

        when(service.createObra(any(Obra.class), anyList(), any(Integer.class), any(Integer.class), any(Endereco.class), anyList() ))
            .thenReturn(obra);

        // Then
        mvc.perform(post("/api/obras")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", is(obra.getId())));
    }

    @Test
    @Disabled
    /* Teste desabilitado pois Mockito nao lanca excessoes que nao sao RuntimeException. Como a exception lancada pela aplicacao
     * e' uma SQLException, o Mockito nao pode fazer o lancamento.
     * Fonte mantido aqui como documentacao de que o back apenas quebra ao inserir obra duplicada.
     */
    public void givenObraDuplicada_whenCreateObra_thenThrowsException() throws Exception {

        // Given
        ObraBuilder builder = new ObraBuilder();
        String json = builder.jsonBuild();
        assertNotNull(json, "Json vazio!");

        when(service.createObra(any(Obra.class), anyList(), any(Integer.class), any(Integer.class), any(Endereco.class), anyList() ))
            .thenThrow( new java.sql.SQLIntegrityConstraintViolationException("Duplicate entry 'Edifício Azevedo Villares' for key 'obra.unique_nome_oficial'" ) );

        // Then
        mvc.perform(post("/api/obras")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isInternalServerError());
        }

    /* TESTES GET /api/obras */

    @Test
    public void givenObraCadastrada_whenGetObras_thenReturnObras() throws Exception {

        // Given
        PageRequest paginacao = PageRequest.of(0, 1);
        Obra obra = new ObraBuilder().build();
        List<Obra> lista_obras = new ArrayList<Obra>(Arrays.asList(obra));
        Page<Obra> result = new PageImpl<Obra>(lista_obras, paginacao, lista_obras.size());

        when(service.readObras(any()))
            .thenReturn(result);

        // Then
        mvc.perform(get("/api/obras"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content", hasSize(1)))
            .andExpect(jsonPath("$.content[0].id", is(obra.getId())));
    }

    /* TESTES GET /api/obras/{id} */

    @Test
    public void givenObraCadastrada_whenGetObraId_thenReturnFound() throws Exception {

        // Given
        Obra obra = new ObraBuilder().build();
        Optional<Obra> result = Optional.of(obra);

        when(service.readObra(any()))
            .thenReturn(result);

        // Then
        mvc.perform(get("/api/obras/1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(obra.getId())));
    }

    @Test
    public void givenObraNaoCadastrada_whenGetObraId_thenReturnNotFound() throws Exception {

        // Given
        Optional<Obra> result = Optional.empty();

        when(service.readObra(any()))
            .thenReturn(result);

        // Then
        mvc.perform(get("/api/obras/1"))
            .andExpect(status().isNotFound());
    }
    @Test
    public void givenObraId_whenUpdateObra_returnOk() throws Exception{
        ObraUpdateDtoBuilder dtoBuilder = new ObraUpdateDtoBuilder();
        ObraBuilder builder = new ObraBuilder();
        Obra obra = builder.build();
        String json = dtoBuilder.jsonBuild();
        assertNotNull(json, "Json vazio!");
        assertNotNull(obra, "Obra vazia!");
        Optional<Obra> result = Optional.of(obra);

        when(service.updateObra(any(Integer.class), any(ObraUpdateDto.class), any(Endereco.class)))
                .thenReturn(result);

        // Then
        mvc.perform(put("/api/obras/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nomeOficial", is(obra.getNomeOficial())))
                .andExpect(jsonPath("$.id", is(obra.getId())));
    }
    @Test
    public void givenObraId_whenDeleteObra_returnEmptyBody() throws Exception
    {
        doNothing().when(service).deleteObra(any(Integer.class));
        mvc.perform(delete("/api/obras/1"))
                .andExpect(status().isNoContent());
    }
}